module HW3
       ( foldr'
       , groupBy'
       , Either' (..)
       , either'
       , lefts'
       , rights'
       ) where

-- foldr' - правая свёртка
-- foldr' + b0 [an,...,a1,a0] =
--    = (an + ... + (a1 + (a0 + b0)))
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f b [] = b
foldr' f b (x:xs) = f x (foldr' f b xs)

-- Группировка
-- > groupBy' (==) [1,2,2,2,1,1,3]
-- [[1],[2,2,2],[1,1],[3]]
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' _ [] = []
groupBy' f (x:xs) = (x:ys) : groupBy' f zs
	       where (ys, zs) = span (f x) xs

-- Сумма типов a и b
data Either' a b = Left' a
                 | Right' b
                 deriving (Show)
-- Например, Either String b -
-- может быть результат типа b
-- или ошибка типа String

-- Из функций (a -> c) и (b -> c) получить функцию
-- над суммой a и b
-- Например,
-- > either' length id (Left' [1,2,3])
-- 3
-- > either' length id (Right' 4)
-- 4
-- > :t either' length id
-- Either [Int] Int -> Int
either' :: (a -> c) -> (b -> c) -> Either' a b -> c
either' x y (Left' c) = x c
either' x y (Right' c) = y c

-- Получение значений соответствующих слагаемых
lefts' :: [Either' a b] -> [a]
lefts' [] = []
lefts' (Left' x : xs) = x : lefts' xs
lefts' (Right' _ : xs) = lefts' xs

rights' :: [Either' a b] -> [b]
rights' [] = []
rights' (Left' _ : xs) = rights' xs
rights' (Right' x : xs) = x : rights' xs