-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown On = True
isKnown Off = True
isKnown Unknown = False

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Sub a b) = eval a - eval b
eval (Add a b) = eval a + eval b
eval (Mult a b) = eval a * eval b
eval (Const a) = a

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const a) = Const a
simplify (Sub a b) = Sub (simplify a) (simplify b)
simplify (Add a b) = Add (simplify a) (simplify b)
simplify (Mult (Add a b) c) = simplify (Add (Mult a c) (Mult b c))
simplify (Mult (Sub a b) c) = simplify (Sub (Mult a c) (Mult b c))
simplify (Mult a (Add b c)) = simplify (Add (Mult a b) (Mult a c))
simplify (Mult a (Sub b c)) = simplify (Sub (Mult a b) (Mult a c))
simplify (Mult a b) = simpl(Mult (simplify a) (simplify b))
simpl (Mult (Add a b) c) = simplify (Add (Mult a c) (Mult b c))
simpl (Mult a (Add b c)) = simplify (Add (Mult a b) (Mult a c))
simpl (Mult (Sub a b) c) = simplify (Sub (Mult a c) (Mult b c))
simpl (Mult a (Sub b c)) = simplify (Sub (Mult a b) (Mult a c))
simpl term = term

